# Define your constants here
LS_DEFAULT_LIMIT = 500
LS_DEFAULT_GLOBAL_LIMIT = 50
LS_STATE_FILE_KEY = "authorized_site"

# URL and Endpoints
LANSWEEPER_BASE_URL = "https://api.lansweeper.com/api/v2"
LANSWEEPER_QUERY_ENDPOINT = "/graphql"

# Constants relating to 'validate_integer'
LS_INT_NOT_VALID = "Please Provide a valid integer value for {param}"
LS_INT_NON_NEG_NON_ZERO = (
    "Please provide a valid non-zero positive integer value for {param}"
)
LS_INT_NON_NEG = "Please provide a valid non-negative integer value for {param}"

# Constants relating to success messages
LS_CONNECTING_ENDPOINT = "Connecting to endpoint"
LS_TEST_SUCCESS_CONN_ENDPOINT = "Test Connectivity Passed"

# Constants relating to error messages
LS_LIST_AUTHORIZED_SITES_FAILED = "List Authorized Sites Failed"
LS_NO_AUTHORIZED_SITE = "No authorized sites found"
LS_TEST_FAILED_CONN_ENDPOINT = "Test Connectivity Failed"
LS_NO_ASSET_INFO = "No Asset Info found"
LS_BAD_IP = "Please Enter valid IP"
LS_NO_AUTHORIZED_SITES = "No authorized sites found"
LS_MORE_PARAM = (
    "Please Provide only one parameter(IP Address, MAC Address, Name, Type or Domain)"
)
LS_SITE_ID_REQUIRED = "Please Provide Site ID"

# State file error messages
LS_STATE_FILE_ERROR = "State File Not Created"
LS_CORRUPT_STATE_FILE = (
    "Error occurred while loading the state file due to its unexpected format. "
    "Resetting the state file with the default format. Please try again."
)

# Validating IP and MAC
LS_INVALID_IP = "Please enter valid IP Address"
LS_INVALID_MAC = "Please enter valid MAC Address"
