#!/usr/bin/python
# -*- coding: utf-8 -*-
# -----------------------------------------
# Phantom sample App Connector python file
# -----------------------------------------

# Python 3 Compatibility imports
from __future__ import print_function, unicode_literals


# Phantom App imports
import phantom.app as phantom
from phantom.base_connector import BaseConnector
from phantom.action_result import ActionResult


# Usage of the consts file is recommended
from lansweeperitam_consts import *
import requests
import json
from bs4 import BeautifulSoup


class RetVal(tuple):
    def __new__(cls, val1, val2=None):
        return tuple.__new__(RetVal, (val1, val2))


class LansweeperItamConnector(BaseConnector):
    def __init__(self):
        """
        Initialize global variables.
        """

        # Call the BaseConnectors init first
        super(LansweeperItamConnector, self).__init__()

        self._base_url = LANSWEEPER_BASE_URL
        self._state = {}
        self._headers = dict()

        # Variable to hold a base_url in case the app makes REST calls
        # Do note that the app json defines the asset config, so please
        # modify this as you deem fit.

    def _process_empty_response(self, response, action_result):
        """
        Process empty response.
        response: response object
        action_result: object of Action Result
        return: status phantom.APP_ERROR/phantom.APP_SUCCESS(along with appropriate message)
        """
        if response.status_code == 200:
            return RetVal(phantom.APP_SUCCESS, {})

        return RetVal(
            action_result.set_status(
                phantom.APP_ERROR, "Empty response and no information in the header"
            ),
            None,
        )

    def _process_html_response(self, response, action_result):
        """
        Process html response.
        response: response object
        action_result: object of Action Result
        return: status phantom.APP_ERROR/phantom.APP_SUCCESS(along with appropriate message)
        """
        # An html response, treat it like an error
        status_code = response.status_code

        try:
            soup = BeautifulSoup(response.text, "html.parser")
            error_text = soup.text
            split_lines = error_text.split("\n")
            split_lines = [x.strip() for x in split_lines if x.strip()]
            error_text = "\n".join(split_lines)
        except Exception:
            error_text = "Cannot parse error details"

        message = "Status Code: {0}. Data from server:\n{1}\n".format(
            status_code, error_text
        )

        message = message.replace("{", "{{").replace("}", "}}")
        return RetVal(action_result.set_status(phantom.APP_ERROR, message), None)

    def _process_json_response(self, r, action_result):
        """
        Process json response.
        r: response object
        action_result: object of Action Result
        return: status phantom.APP_ERROR/phantom.APP_SUCCESS(along with appropriate message)
        """
        # Try a json parse
        status_code = r.status_code
        try:
            resp_json = r.json()
        except Exception as e:
            return RetVal(
                action_result.set_status(
                    phantom.APP_ERROR,
                    "Unable to parse JSON response. Error: {0}".format(str(e)),
                ),
                None,
            )

        errors = resp_json.get("errors")
        if errors and isinstance(errors, list):
            messages = ""
            for error in errors:
                msg = error.get("message", "Error message unavailable")
                messages = "{0} {1}".format(messages, msg)
            return RetVal(
                action_result.set_status(
                    phantom.APP_ERROR,
                    "Error: {}".format(messages),
                ),
                resp_json,
            )
        # Please specify the status codes here
        if 200 <= status_code < 399:
            return RetVal(phantom.APP_SUCCESS, resp_json)

        # You should process the error returned in the json
        message = "Error from server. Status Code: {0} Data from server: {1}".format(
            r.status_code, r.text.replace("{", "{{").replace("}", "}}")
        )

        return RetVal(action_result.set_status(phantom.APP_ERROR, message), None)

    def _process_response(self, r, action_result):
        """
        Process response.
        r: response object
        action_result: object of Action Result
        return: status phantom.APP_ERROR/phantom.APP_SUCCESS(along with appropriate message)
        """
        # store the r_text in debug data, it will get dumped in the logs if the action fails
        if hasattr(action_result, "add_debug_data"):
            action_result.add_debug_data({"r_status_code": r.status_code})
            action_result.add_debug_data({"r_text": r.text})
            action_result.add_debug_data({"r_headers": r.headers})

        # Process each 'Content-Type' of response separately

        # Process a json response
        if "json" in r.headers.get("Content-Type", ""):
            return self._process_json_response(r, action_result)

        # Process an HTML response, Do this no matter what the api talks.
        # There is a high chance of a PROXY in between phantom and the rest of
        # world, in case of errors, PROXY's return HTML, this function parses
        # the error and adds it to the action_result.
        if "html" in r.headers.get("Content-Type", ""):
            return self._process_html_response(r, action_result)

        # it's not content-type that is to be parsed, handle an empty response
        if not r.text:
            return self._process_empty_response(r, action_result)

        # everything else is actually an error at this point
        message = "Can't process response from server. Status Code: {0} Data from server: {1}".format(
            r.status_code, r.text.replace("{", "{{").replace("}", "}}")
        )

        return RetVal(action_result.set_status(phantom.APP_ERROR, message), None)

    def _make_rest_call(self, endpoint, action_result, method="get", **kwargs):
        """
        Make REST call.
        endpoint: api endpoint
        method: GET/POST/PUT/DELETE/PATCH (Default will be GET)
        action_result: object of Action Result
        return: status phantom.APP_ERROR/phantom.APP_SUCCESS(along with appropriate message)
        response obtained by making an API call
        """
        # **kwargs can be any additional parameters that requests.request accepts

        config = self.get_config()
        headers = self._headers

        resp_json = None

        try:
            request_func = getattr(requests, method)
        except AttributeError:
            return RetVal(
                action_result.set_status(
                    phantom.APP_ERROR, "Invalid method: {0}".format(method)
                ),
                resp_json,
            )

        # Create a URL to connect to
        url = "{0}{1}".format(self._base_url, endpoint)

        try:
            r = request_func(
                url,
                verify=config.get("verify_server_cert", True),
                headers=headers,
                **kwargs,
            )
        except Exception as e:
            return RetVal(
                action_result.set_status(
                    phantom.APP_ERROR,
                    "Error Connecting to server. Details: {0}".format(str(e)),
                ),
                resp_json,
            )

        return self._process_response(r, action_result)

    def _handle_test_connectivity(self, param):
        """
        Validate the asset configuration for connectivity using provided configuration.
        param: dictionary of input parameters
        return: status(phantom.APP_SUCCESS/phantom.APP_ERROR)
        """
        # Add an action result object to self (BaseConnector) to represent the action for this param
        action_result = self.add_action_result(ActionResult(dict(param)))

        # Note: test connectivity does _NOT_ take any parameters
        # i.e. the param dictionary passed to this handler will be empty.
        # Also typically it does not add any data into an action_result either.
        # The status and progress messages are more important.

        self.save_progress(LS_CONNECTING_ENDPOINT)

        # call list authorized sites
        ret_val = self._handle_list_authorized_sites(param, action_result)

        if phantom.is_fail(ret_val):
            # the call to the 3rd party device or service failed, action result should contain all the error details
            self.save_progress(LS_TEST_FAILED_CONN_ENDPOINT)
            return action_result.get_status()

        # Return success
        self.save_progress(LS_TEST_SUCCESS_CONN_ENDPOINT)

        return action_result.set_status(phantom.APP_SUCCESS)

    def _handle_list_authorized_sites(self, param, action_result=None):
        """
        List authorized sites.
        param: dictionary of input parameters
        action_result: Object of ActionResult class
        return: status(phantom.APP_SUCCESS/phantom.APP_ERROR)
        """
        # Implement the handler here
        # use self.save_progress(...) to send progress messages back to the platform

        if not action_result:
            action_result = self.add_action_result(ActionResult(dict(param)))

        query = """query {
            authorizedSites {
                sites {
                    id
                    name
                }
            }
        }"""

        # Make rest call
        ret_val, response = self._make_rest_call(
            action_result=action_result,
            endpoint=LANSWEEPER_QUERY_ENDPOINT,
            json={"query": query},
            method="post",
        )

        if phantom.is_fail(ret_val):
            self.save_progress(LS_LIST_AUTHORIZED_SITES_FAILED)
            return action_result.get_status()

        sites = response.get("data", {}).get("authorizedSites", {}).get("sites")

        if sites:
            for site in sites:
                action_result.add_data(site)
        else:
            return action_result.set_status(phantom.APP_SUCCESS, LS_NO_AUTHORIZED_SITE)

        # self._state = self.load_state()
        self._state[LS_STATE_FILE_KEY] = sites
        self.save_state(self._state)

        action_result.update_summary({"total_sites": len(sites)})

        return action_result.set_status(phantom.APP_SUCCESS)

    def _validate_mac(self, mac):
        """
        Validating the MAC Address
        mac: MAC Address of Asset
        return: MAC Address valid or not
        """
        import macaddress

        # It supports multiple format of mac address
        class MACAllowsTrailingDelimiters(macaddress.MAC):
            formats = macaddress.MAC.formats + (
                "xx-xx-xx-xx-xx-xx-",
                "xx:xx:xx:xx:xx:xx:",
                "xxxx.xxxx.xxxx.",
            )

        # Validating the mac address

        try:
            MACAllowsTrailingDelimiters(mac)
        except Exception:
            return False
        return True

    def _comma_seperated_mac(self, mac, action_result=None):
        """
        Filter the comma seperated values in the mac. This method operates in steps:
        1. Get list with comma as the seperator
        2. Filter valid mac from the list
        3. Filter empty values from the list
        action_result: Action result object
        mac: input parameter
        return: status phantom.APP_ERROR/phantom.APP_SUCCESS, filtered list or None in case of failure
        """
        valid_macs = []
        mac_list = mac.split(",")

        for macs in mac_list:

            # Validation of MAC address
            if not self._validate_mac(macs):
                continue
            else:
                valid_macs.append(macs)

        # If no mac address found or all mac addresses are invalid
        if not valid_macs:
            return (
                action_result.set_status(phantom.APP_ERROR, LS_INVALID_MAC),
                valid_macs,
            )

        return action_result.set_status(phantom.APP_SUCCESS), valid_macs

    def _validate_ip(self, ip):
        """
        Validating the IP Address
        ip: IP Address of Asset
        return: IP Address valid or not
        """
        import ipaddress

        # Validating the ip address
        try:
            ip = ipaddress.ip_address(ip)
        except Exception:
            return False
        else:
            return True

    def _comma_seperated_ip(self, ip, action_result=None):
        """
        Filter the comma seperated values in the ip. This method operates in steps:
        1. Get list with comma as the seperator
        2. Filter valid ip from the list
        3. Filter empty values from the list
        action_result: Action result object
        ip: input parameter
        return: status phantom.APP_ERROR/phantom.APP_SUCCESS, filtered list or None in case of failure
        """

        valid_ips = []
        ip_list = ip.split(",")

        for ips in ip_list:
            self.debug_print(f"inside comma ip value {ips} .....")
            # Validation of IP address
            if not self._validate_ip(ips):
                continue
            else:
                valid_ips.append(ips)

        # If no ip address found or all ip addresses are invalid
        if not valid_ips:
            self.debug_print(f"inside comma invalid ip value {valid_ips} .....")
            return (
                action_result.set_status(phantom.APP_ERROR, LS_INVALID_IP),
                valid_ips,
            )

        return action_result.set_status(phantom.APP_SUCCESS), valid_ips

    def _get_all_sites(self, action_result=None):
        """
        Store the authorized sites in list from the state file
        action_result: Action result object
        """

        # List all the site ids
        site_ids = self._state[LS_STATE_FILE_KEY]
        list_of_sites = [site["id"] for site in site_ids]

        # If no authorized sites found
        if not list_of_sites:
            return (
                action_result.set_status(phantom.APP_ERROR, LS_NO_AUTHORIZED_SITES),
                list_of_sites,
            )

        return action_result.set_status(phantom.APP_SUCCESS), list_of_sites

    def _comma_seperated_sites(self, items, action_result=None):
        """
        Filter the comma seperated values in the sites. This method operates in steps:
        1. Get list with comma as the seperator
        2. Filter empty values from the list
        action_result: Action result object
        items: input parameter
        return: status phantom.APP_ERROR/phantom.APP_SUCCESS, filtered list or None in case of failure
        """

        list_of_sites = []

        if not items:
            # If the site ids list is empty
            ret_val, list_of_sites = self._get_all_sites(action_result)

            if phantom.is_fail(ret_val):
                # the call to the 3rd party device or service failed, action result should contain all the error details
                return action_result.get_status()

        else:
            # If multiple site ids are provided
            sites_list = items.split(",")
            for sites in sites_list:
                if not sites:
                    continue
                else:
                    list_of_sites.append(sites)

            if not list_of_sites:
                # If the site ids list is empty
                ret_val, list_of_sites = self._get_all_sites(action_result)

        return action_result.set_status(phantom.APP_SUCCESS), list_of_sites

    def _validate_integer(self, action_result, parameter, key, allow_zero=False):
        """
        Validate an integer.
        action_result: Action result or BaseConnector object
        parameter: input parameter
        allow_zero: whether zero should be considered as valid value or not
        return: status phantom.APP_ERROR/phantom.APP_SUCCESS, integer value of the parameter or None in case of failure
        """
        if parameter is not None:
            try:
                if not float(parameter).is_integer():
                    return (
                        action_result.set_status(
                            phantom.APP_ERROR, LS_INT_NOT_VALID.format(param=key)
                        ),
                        None,
                    )

                parameter = int(parameter)
            except Exception:
                return (
                    action_result.set_status(
                        phantom.APP_ERROR, LS_INT_NOT_VALID.format(param=key)
                    ),
                    None,
                )

            if parameter < 0:
                return (
                    action_result.set_status(
                        phantom.APP_ERROR, LS_INT_NON_NEG.format(param=key)
                    ),
                    None,
                )

            if not allow_zero and parameter == 0:
                return (
                    action_result.set_status(
                        phantom.APP_ERROR, LS_INT_NON_NEG_NON_ZERO.format(param=key)
                    ),
                    None,
                )

        return action_result.set_status(phantom.APP_SUCCESS), parameter

    def _pagination(self, action_result, query, input_limit):

        """
        Pagination
        input_limit : No. of data user want
        query: GraphQl query
        action_result: Action result object
        """
        max_limit = input_limit

        # Fetching the first page
        variables = {
            "pagination": {
                "limit": min(input_limit, LS_DEFAULT_LIMIT),
                "page": "FIRST",
            }
        }

        list_items = []

        while True:

            # make rest call
            ret_val, response = self._make_rest_call(
                action_result=action_result,
                endpoint=LANSWEEPER_QUERY_ENDPOINT,
                json={"query": query, "variables": variables},
                method="post",
            )

            if phantom.is_fail(ret_val):
                # the call to the 3rd party device or service failed, action result should contain all the error details
                return action_result.get_status()

            # For fetching the items from response
            items = (
                response.get("data", {})
                .get("site", {})
                .get("assetResources", {})
                .get("items", [])
            )

            # Adding items to list
            list_items.extend(items)

            # total items fetched is less then default limit then exit from the loop
            if len(items) < LS_DEFAULT_LIMIT:
                break

            # Max results fetched. Hence, exit the paginator.
            if len(list_items) == max_limit:
                return phantom.APP_SUCCESS, list_items[:max_limit]

            # Fetching next value for cursor in pagination
            next = (
                response.get("data", {})
                .get("site", {})
                .get("assetResources", {})
                .get("pagination", {})
                .get("next")
            )

            if not next:
                break

            # Removing the items count from the limit provided by the user

            input_limit = input_limit - len(items)

            variables = {
                "pagination": {
                    "limit": min(input_limit, LS_DEFAULT_LIMIT),
                    "page": "NEXT",
                    "cursor": next,
                }
            }

        return phantom.APP_SUCCESS, list_items

    def _parameter_count(self, input_param):
        """
        Counts the number of input parameters
        input_param: dictionary of input parameters
        return: Count is 1 or not, Dictionary key and value
        """
        output_key, output_value = None, None
        count = 0
        for input_key, input_value in input_param.items():
            if input_value:
                count += 1
                output_key = input_key
                output_value = input_value
            if count > 1:
                return False, None, None
        return True, output_key, output_value

    def _handle_hunt_ip(self, param):
        """
        Fetch asset details based on the IP address.
        param: dictionary of input parameters
        return: status(phantom.APP_SUCCESS/phantom.APP_ERROR)
        """
        # Implement the handler here
        # use self.save_progress(...) to send progress messages back to the platform
        self.save_progress(
            "In action handler for: {0}".format(self.get_action_identifier())
        )
        self.debug_print("inside Hunt ip .....")
        # Add an action result object to self (BaseConnector) to represent the action for this param
        action_result = self.add_action_result(ActionResult(dict(param)))

        # Access action parameters passed in the 'param' dictionary
        input_limit = param.get("limit", LS_DEFAULT_LIMIT)
        ret_val, input_limit = self._validate_integer(
            action_result=action_result, parameter=input_limit, key="limit"
        )

        if phantom.is_fail(ret_val):
            # the call to the 3rd party device or service failed, action result should contain all the error details
            return action_result.get_status()

        input_global_limit = param.get("global_limit", LS_DEFAULT_GLOBAL_LIMIT)
        ret_val, input_global_limit = self._validate_integer(
            action_result=action_result,
            parameter=input_global_limit,
            key="global limit",
        )

        if phantom.is_fail(ret_val):
            # the call to the 3rd party device or service failed, action result should contain all the error details
            return action_result.get_status()

        string_condition = ""
        # Required values can be accessed directly
        ip_input = param["ip"]
        # Optional values should use the .get() function
        site_id = param.get("site_id", "")

        ret_val, sites = self._comma_seperated_sites(site_id, action_result)

        if phantom.is_fail(ret_val):
            # the call to the 3rd party device or service failed, action result should contain all the error details
            return action_result.get_status()

        ret_val, ip_list = self._comma_seperated_ip(ip_input, action_result)
        self.debug_print(f"inside hunt ip ip value {ip_list} .....")

        if phantom.is_fail(ret_val):
            # the call to the 3rd party device or service failed, action result should contain all the error details
            return action_result.get_status()

        for ip in ip_list:
            # Condition for fetching the ip addresses
            string_condition = string_condition + (
                """{
                                operator: EQUAL,
                                path: "assetBasicInfo.ipAddress",
                                value: "%s"
                            },"""
                % ip
            )

        total_ips = []

        for site in sites:

            query = """query getAssetResources($pagination: AssetsPaginationInputValidated) {
                    site(id: "%s") {
                        assetResources (
                        assetPagination: $pagination,
                        fields:
                        [
                            "assetBasicInfo.name",
                            "assetBasicInfo.mac",
                            "assetCustom.model",
                            "assetBasicInfo.type",
                            "assetBasicInfo.domain",
                            "assetBasicInfo.ipAddress",
                            "assetCustom.stateName",
                            "assetCustom.manufacturer",
                            "installKey",
                            "assetCustom.purchaseDate",
                            "assetCustom.warrantyDate",
                            "assetBasicInfo.firstSeen"
                        ],
                        filters:
                        {
                            conjunction: OR,
                            conditions: [%s]
                        }
                        ) {
                            total
                            pagination {
                                limit
                                current
                                next
                                page
                            }
                            items
                        }
                    }
                }""" % (
                site,
                string_condition,
            )

            # pagination
            ret_val, asset_item = self._pagination(action_result, query, input_limit)

            if phantom.is_fail(ret_val):
                # the call to the 3rd party device or service failed, action result should contain all the error details
                return action_result.get_status()

            if asset_item:
                for item in asset_item:
                    total_ips.append(item)
            else:
                return action_result.set_status(phantom.APP_SUCCESS, LS_NO_ASSET_INFO)

        # Add the response into the data section
        ip_assets = total_ips[:input_global_limit]
        # Add the response into the data section
        # for item_of_mac_list in range(no_of_assets):
        action_result.add_data(ip_assets)

        # Add a dictionary that is made up of the most important values from data into the summary
        action_result.update_summary({"total assets": len(ip_assets)})

        # Return success, no need to set the message, only the status
        # BaseConnector will create a textual message based off of the summary dictionary
        return action_result.set_status(phantom.APP_SUCCESS)

    def _handle_hunt_mac(self, param):
        """
        Fetch asset details based on the MAC address.
        param: dictionary of input parameters
        return: status(phantom.APP_SUCCESS/phantom.APP_ERROR)
        """
        # Implement the handler here
        # use self.save_progress(...) to send progress messages back to the platform
        self.save_progress(
            "In action handler for: {0}".format(self.get_action_identifier())
        )

        # Add an action result object to self (BaseConnector) to represent the action for this param
        action_result = self.add_action_result(ActionResult(dict(param)))

        # Access action parameters passed in the 'param' dictionary
        input_limit = param.get("limit", LS_DEFAULT_LIMIT)
        ret_val, input_limit = self._validate_integer(
            action_result=action_result, parameter=input_limit, key="limit"
        )

        if phantom.is_fail(ret_val):
            # the call to the 3rd party device or service failed, action result should contain all the error details
            return action_result.get_status()

        input_global_limit = param.get("global_limit", LS_DEFAULT_GLOBAL_LIMIT)
        ret_val, input_global_limit = self._validate_integer(
            action_result=action_result,
            parameter=input_global_limit,
            key="global limit",
        )

        if phantom.is_fail(ret_val):
            # the call to the 3rd party device or service failed, action result should contain all the error details
            return action_result.get_status()

        string_condition = ""
        # Required values can be accessed directly
        mac_input = param["mac"]
        # Optional values should use the .get() function
        site_id = param.get("site_id", "")

        ret_val, sites = self._comma_seperated_sites(site_id, action_result)

        if phantom.is_fail(ret_val):
            # the call to the 3rd party device or service failed, action result should contain all the error details
            return action_result.get_status()

        ret_val, mac_list = self._comma_seperated_mac(mac_input, action_result)

        if phantom.is_fail(ret_val):
            # the call to the 3rd party device or service failed, action result should contain all the error details
            return action_result.get_status()

        for mac in mac_list:
            # Condition for fetching the mac addresses
            string_condition = string_condition + (
                """{
                                    operator: EQUAL,
                                    path: "assetBasicInfo.mac",
                                    value: "%s"
                                },"""
                % mac
            )

        total_macs = []

        for site in sites:

            query = """query getAssetResources($pagination: AssetsPaginationInputValidated) {
                    site(id: "%s") {
                        assetResources (
                        assetPagination: $pagination,
                        fields:
                        [
                            "assetBasicInfo.name",
                            "assetBasicInfo.mac",
                            "assetCustom.model",
                            "assetBasicInfo.type",
                            "assetBasicInfo.domain",
                            "assetBasicInfo.ipAddress",
                            "assetCustom.stateName",
                            "assetCustom.manufacturer",
                            "installKey",
                            "assetCustom.purchaseDate",
                            "assetCustom.warrantyDate",
                            "assetBasicInfo.firstSeen"
                        ],
                        filters:
                        {
                            conjunction: OR,
                            conditions: [%s]
                        }
                        ) {
                            total
                            pagination {
                                limit
                                current
                                next
                                page
                            }
                            items
                        }
                    }
                }""" % (
                site,
                string_condition,
            )

            # pagination
            ret_val, asset_item = self._pagination(action_result, query, input_limit)

            if phantom.is_fail(ret_val):
                # the call to the 3rd party device or service failed, action result should contain all the error details
                return action_result.get_status()

            if asset_item:
                for item in asset_item:
                    total_macs.append(item)
            else:
                return action_result.set_status(phantom.APP_SUCCESS, LS_NO_ASSET_INFO)
        # Now post process the data,  uncomment code as you deem fit
        mac_assets = total_macs[:input_global_limit]
        # Add the response into the data section
        # for item_of_mac_list in range(no_of_assets):
        action_result.add_data(mac_assets)

        # Add a dictionary that is made up of the most important values from data into the summary
        action_result.update_summary({"total assets": len(mac_assets)})

        # Return success, no need to set the message, only the status
        # BaseConnector will create a textual message based off of the summary dictionary
        return action_result.set_status(phantom.APP_SUCCESS)

    def _handle_run_query(self, param):
        """
        Fetch asset details based on the query or input parameter.
        param: dictionary of input parameters
        return: status(phantom.APP_SUCCESS/phantom.APP_ERROR)
        """
        # Implement the handler here
        # use self.save_progress(...) to send progress messages back to the platform
        self.save_progress(
            "In action handler for: {0}".format(self.get_action_identifier())
        )

        # Add an action result object to self (BaseConnector) to represent the action for this param
        action_result = self.add_action_result(ActionResult(dict(param)))

        user_query = param.get("user_query")
        mac_input = param.get("mac")
        ip_input = param.get("ip")
        name_input = param.get("name")
        type_input = param.get("type")
        domain_input = param.get("domain")
        input_limit = param.get("limit", LS_DEFAULT_LIMIT)

        input_parameters = {
            "ipAddress": ip_input,
            "mac": mac_input,
            "name": name_input,
            "type": type_input,
            "domain": domain_input,
        }

        count_value, input_key, input_value = self._parameter_count(input_parameters)

        if user_query:

            # make rest call
            ret_val, response = self._make_rest_call(
                action_result=action_result,
                endpoint=LANSWEEPER_QUERY_ENDPOINT,
                json={"query": user_query},
                method="post",
            )

            if phantom.is_fail(ret_val):
                # the call to the 3rd party device or service failed, action result should contain all the error details
                return action_result.get_status()

            access_items = (
                response.get("data", {})
                .get("site", {})
                .get("assetResources", {})
                .get("items")
            )

            query_result = response.get("data", {})

            if access_items:
                for item_asset in access_items:
                    action_result.add_data(item_asset)
            else:
                return action_result.set_status(phantom.APP_SUCCESS, LS_NO_ASSET_INFO)
            # Add a dictionary that is made up of the most important values from data into the summary
            action_result.update_summary(
                {
                    "total assets": len(access_items),
                    "query response result": query_result,
                }
            )

        else:

            site_id = param.get("site_id")

            if site_id:

                if count_value:

                    ret_val, input_limit = self._validate_integer(
                        action_result, input_limit, key="limit"
                    )

                    if phantom.is_fail(ret_val):
                        # the call to the 3rd party device or service failed, action result should contain all the error details
                        return action_result.get_status()

                    if mac_input:
                        # Validating MAC
                        if not self._validate_mac(mac_input):
                            action_result.set_status(phantom.APP_ERROR, LS_INVALID_MAC)
                            return action_result.get_status()

                    if ip_input:
                        # Validating IP
                        if not self._validate_ip(ip_input):
                            action_result.set_status(phantom.APP_ERROR, LS_INVALID_IP)
                            return action_result.get_status()

                    # for input_key, input_value in input_parameters.items():

                    if input_value:

                        query = """query getAssetResources($pagination: AssetsPaginationInputValidated) {
                                    site(id: "%s") {
                                        assetResources (
                                        assetPagination: $pagination,
                                        fields:
                                        [
                                            "assetBasicInfo.name",
                                            "assetBasicInfo.mac",
                                            "assetCustom.model",
                                            "assetBasicInfo.type",
                                            "assetBasicInfo.domain",
                                            "assetBasicInfo.ipAddress",
                                            "assetCustom.stateName",
                                            "assetCustom.manufacturer",
                                            "installKey",
                                            "assetCustom.purchaseDate",
                                            "assetCustom.warrantyDate",
                                            "assetBasicInfo.firstSeen"
                                        ],
                                        filters:
                                        {
                                            conjunction: OR,
                                            conditions: [
                                                {
                                                operator: EQUAL,
                                                path: "assetBasicInfo.%s",
                                                value: "%s"
                                            }
                                            ]
                                        }
                                        ) {
                                            pagination {
                                                next
                                            }
                                            items
                                        }
                                    }
                                }""" % (
                            site_id,
                            input_key,
                            input_value,
                        )

                        # pagination
                        ret_val, asset_item = self._pagination(
                            action_result, query, input_limit
                        )

                        if phantom.is_fail(ret_val):
                            # the call to the 3rd party device or service failed, action result should contain all the error details
                            return action_result.get_status()

                        if asset_item:
                            action_result.add_data(asset_item)
                            # Add a dictionary that is made up of the most important values from data into the summary
                            action_result.update_summary(
                                {"total assets": len(asset_item)}
                            )

                        else:
                            return action_result.set_status(
                                phantom.APP_SUCCESS, LS_NO_ASSET_INFO
                            )
                        # Add the response into the data section
                        # action_result.add_data(asset_item)
                        # # Add a dictionary that is made up of the most important values from data into the summary
                        # action_result.update_summary({"total assets": len(asset_item)})
                else:
                    action_result.set_status(phantom.APP_ERROR, LS_MORE_PARAM)
                    return action_result.get_status()
            else:
                action_result.set_status(phantom.APP_ERROR, LS_SITE_ID_REQUIRED)
                return action_result.get_status()
        # else:
        #     action_result.set_status(phantom.APP_ERROR, LS_MORE_PARAM)
        #     return action_result.get_status()
        # Return success, no need to set the message, only the status
        # BaseConnector will create a textual message based off of the summary dictionary
        return action_result.set_status(phantom.APP_SUCCESS)

    def handle_action(self, param):
        """
        Get current action identifier and call member function of its own to handle the action.
        param: dictionary which contains information about the actions to be executed
        return: status success/failure
        """
        self.debug_print("inside Handle action 0.....")
        ret_val = phantom.APP_SUCCESS

        # Get the action that we are supposed to execute for this App Run
        action_id = self.get_action_identifier()
        self.debug_print("inside Handle action 1.....")
        self.debug_print("action_id", self.get_action_identifier())

        if action_id == "test_connectivity":
            ret_val = self._handle_test_connectivity(param)

        elif action_id == "list_authorized_sites":
            ret_val = self._handle_list_authorized_sites(param)

        elif action_id == "hunt_ip":
            ret_val = self._handle_hunt_ip(param)

        elif action_id == "hunt_mac":
            ret_val = self._handle_hunt_mac(param)

        elif action_id == "run_query":
            ret_val = self._handle_run_query(param)

        return ret_val

    def initialize(self):
        """"
        Initialize the global variables with its value and validate it.\
        return: status (success/failure)
        """
        # Load the state in initialize, use it to store data
        # that needs to be accessed across actions

        self._state = self.load_state()
        self.debug_print("inside Initialize .....")
        if not isinstance(self._state, dict):
            self.debug_print("Reseting the state file with the default format")
            self._state = {"app_version": self.get_app_json().get("app_version")}
            return self.set_status(phantom.APP_FAILED, LS_CORRUPT_STATE_FILE)

        self.debug_print("inside initialize before config .....")
        # get the asset config
        config = self.get_config()
        self.debug_print("inside initialize after config .....")

        token_id = config["token_id"]

        """
        # Access values in asset config by the name

        # Required values can be accessed directly
        required_config_name = config['required_config_name']


        # Optional values should use the .get() function
        optional_config_name = config.get('optional_config_name')
        """

        self._headers = {
            "Content-Type": "application/json",
            "Authorization": "Token {0}".format(token_id),
        }

        return phantom.APP_SUCCESS

    def finalize(self):
        """ "
        Perform some final operations or clean up operations.
        return: status (success/failure)
        """
        self.debug_print("inside finalize .....")
        # Save the state, this data is saved across actions and app upgrades
        self.save_state(self._state)
        return phantom.APP_SUCCESS


def main():
    import pudb
    import argparse

    pudb.set_trace()

    argparser = argparse.ArgumentParser()

    argparser.add_argument("input_test_json", help="Input Test JSON file")
    argparser.add_argument("-u", "--username", help="username", required=False)
    argparser.add_argument("-p", "--password", help="password", required=False)

    args = argparser.parse_args()
    session_id = None

    username = args.username
    password = args.password

    if username is not None and password is None:

        # User specified a username but not a password, so ask
        import getpass

        password = getpass.getpass("Password: ")

    if username and password:
        try:
            login_url = LansweeperItamConnector._get_phantom_base_url() + "/login"

            print("Accessing the Login page")
            r = requests.get(login_url, verify=False)
            csrftoken = r.cookies["csrftoken"]

            data = dict()
            data["username"] = username
            data["password"] = password
            data["csrfmiddlewaretoken"] = csrftoken

            headers = dict()
            headers["Cookie"] = "csrftoken=" + csrftoken
            headers["Referer"] = login_url

            print("Logging into Platform to get the session id")
            r2 = requests.post(login_url, verify=False, data=data, headers=headers)
            session_id = r2.cookies["sessionid"]
        except Exception as e:
            print("Unable to get session id from the platform. Error: " + str(e))
            exit(1)

    with open(args.input_test_json) as f:
        in_json = f.read()
        in_json = json.loads(in_json)
        print(json.dumps(in_json, indent=4))

        connector = LansweeperItamConnector()
        connector.print_progress_message = True

        if session_id is not None:
            in_json["user_session_token"] = session_id
            connector._set_csrf_info(csrftoken, headers["Referer"])

        ret_val = connector._handle_action(json.dumps(in_json), None)
        print(json.dumps(json.loads(ret_val), indent=4))

    exit(0)


if __name__ == "__main__":
    main()
